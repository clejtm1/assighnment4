package com.example.charles_lejtman_todos_app

interface ITodoController {
    fun deleteTodo(idx: Int)
    fun toggleCompleted(idx: Int)
    val todos:ItodoRepository
}