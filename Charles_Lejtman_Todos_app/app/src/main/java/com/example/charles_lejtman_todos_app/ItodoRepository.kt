package com.example.charles_lejtman_todos_app

interface ItodoRepository {
    fun getCount(): Int
    fun getTodo(idx: Int): to_do_model
    fun getAll(): List<to_do_model>
    fun remove(song: to_do_model)
    fun replace(idx: Int, to_do: to_do_model)
    fun addTodo(todo:to_do_model)
}