package com.example.charles_lejtman_todos_app
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import kotlinx.android.synthetic.main.activity_main.*
import com.google.gson.Gson
import android.widget.Toast
class MainActivity : AppCompatActivity() {

    private fun launchNewToDoActivity()
    {
        val intent = Intent(this, NewToDoActivity::class.java)
        startActivityForResult(intent,ADD_TODO_REQUEST_CODE)
    }
  /*  override fun deleteTodo(idx: Int)
    {
        val current = todos.getTodo(idx)
        todos.remove(current)
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         start_btn.setOnClickListener {launchNewToDoActivity()  }
    }

   /* override lateinit var todos: ItodoRepository */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            ADD_TODO_REQUEST_CODE -> {
                when(resultCode) {
                    Activity.RESULT_OK -> {
                        // handle the result
                        val json = data?.getStringExtra(NewToDoActivity.TO_DO_KEY)

                        val to_do = Gson().fromJson(json, to_do_model::class.java)
                        result_tv.text=to_do.toString()

                        //songs.addSong(song)
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(this, "Use cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
    companion object {
        val ADD_TODO_REQUEST_CODE = 1
    }
}
