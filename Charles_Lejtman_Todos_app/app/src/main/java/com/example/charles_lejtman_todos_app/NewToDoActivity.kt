package com.example.charles_lejtman_todos_app
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_new_todo.*
import com.google.gson.Gson
import java.time.MonthDay

class NewToDoActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)
        save_btn.setOnClickListener{ handleAddToDoClick()}
    }
    private fun handleAddToDoClick()
    {
        val intent = Intent()



        val to_do_model = to_do_model(
            title= title_text.editableText.toString(),
            contents = contents_text.editableText.toString(),
            isCompleted = checkBox.isChecked,
            image= ""
        )

        val json: String = Gson().toJson(to_do_model)

        intent.putExtra(TO_DO_KEY, json)

        setResult(Activity.RESULT_OK, intent)

        finish()
    }
    companion object {
        val TO_DO_KEY = "TO_DO_EXTRA"
    }

}
