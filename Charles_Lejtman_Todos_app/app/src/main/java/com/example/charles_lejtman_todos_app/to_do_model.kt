package com.example.charles_lejtman_todos_app

import java.util.*
import java.time.MonthDay


data class to_do_model (
        val title: String,
        val contents: String,
        val isCompleted: Boolean,
        val image:String
        //val date: String
    )

